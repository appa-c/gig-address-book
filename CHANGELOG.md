# Changelog

## [0.0.2] - 30-10-2017

### Added

- CHANGELOG.md

### Changed

- Fixed JSDoc issues, usually relating to changed or unused parameters.
- Tidied whitespace in main.html

### Removed

- Removed unused parameter '$rootScope' in ListCtrl
- Removed unused CSS class 'sorting'

## [0.0.1] - 29-10-2017

### Added

- README.md
- Contact/Entry functionality
  - Contacts can be added, edit, and removed.
