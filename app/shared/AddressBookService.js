/**
 * Address book service - manages the creation, reading,
 * updating and deletion of address book entires via
 * the localForge module.
 *
 * @param {localForage} $localForage localForge module.
 * @constructor
 */
function AddressBookService($localForage) {
  /**
   * Generates a unique ID using unix timestamp and contact's email address.
   * @param {String} email
   * @return {String} Unique ID.
   */
  function generateUniqueId(email) {
   let id = '';
   id += Date.now().toString(16);
   for (let i = 0, l = email.length; i < l; i++) {
     id += email.charCodeAt(i).toString(16);
   }
   return id;
  }
  return {
    createEntry: (entry, cb) => {
      entry._id = generateUniqueId(entry.email);
      entry._createdAt = Date.now();
      $localForage.setItem(entry._id, entry)
        .then((createdEntry) => {
          cb(null, createdEntry);
        })
        .catch((err) => {
          cb(err);
        });
    },
    readEntry: (id, cb) => {
      $localForage.getItem(id)
        .then((entry) => {
          cb(null, entry);
        })
        .catch((err) => {
          cb(err);
        });
    },
    updateEntry: (id, entry, cb) => {
      entry._updatedAt = Date.now();
      $localForage.setItem(id, entry)
        .then((updatedEntry) => {
          cb(null, updatedEntry);
        })
        .catch((err) => {
          cb(err);
        });
    },
    deleteEntry: (id, cb) => {
      $localForage.removeItem(id)
        .then(() => {
          cb(null, id);
        })
        .catch((err) => {
          cb(err);
        });
    },
    readEntries: (cb) => {
      let entries = [];
      $localForage.iterate((entry, key, iterator) => {
          entries.push(entry);
      })
      .then(() => {
          cb(null, entries);
      })
      .catch((err) => {
        cb(err);
      });
    },
  };
}

angular.module('AddressBook').service('AddressBookService', AddressBookService);
