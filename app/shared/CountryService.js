const countries = require('country-list')();

/**
 * Service for interfacing with 'country-list' module.
 * @return {Array} countries - Returns an array of countries by name.
 */
function CountryService() {
  return {
    getCountries: () => {
      return countries.getNames();
    },
  };
}

angular.module('AddressBook').service('CountryService', CountryService);
