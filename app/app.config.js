/**
 * App configuration.
 * @param {routeProvider} $routeProvider
 * @param {locationProvider} $locationProvider
 * @param {localForageProvider} $localForageProvider
 */
function appConfig($routeProvider, $locationProvider, $localForageProvider) {
  $localForageProvider.setNotify(true, true);
  $locationProvider.html5Mode(true);
  $routeProvider
    .when('/', {
      templateUrl: 'views/list.html',
      controller: 'ListCtrl',
      controllerAs: 'vm',
    })
    .when('/manage', {
      templateUrl: 'views/manage.html',
      controller: 'ManageCtrl',
      controllerAs: 'vm',
    })
    .otherwise({
      redirectTo: '/',
    });
}

angular.module('AddressBook').config(appConfig);
