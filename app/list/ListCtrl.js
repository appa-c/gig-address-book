/**
 * Controller for list view.
 * @param {Object} AddressBookService Address book service for managing entries.
 * @constructor
 */
function ListCtrl(AddressBookService) {
  const vm = this;

  vm.readEntries = () => {
    AddressBookService.readEntries((err, entries) => {
      if (err) {
        console.log(err);
      }
      vm.entries = entries;
    });
  };

  vm.readEntries();
}

angular.module('AddressBook').controller('ListCtrl', ListCtrl);
