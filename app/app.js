'use strict';

// App Core
angular.module('AddressBook', ['ngRoute', 'LocalForageModule']);
require('./app.config');

// Services
require('./shared/AddressBookService.js');
require('./shared/CountryService.js');

// Controllers
require('./list/ListCtrl.js');
require('./manage/ManageCtrl.js');
