/**
 * Controller for the manage address book view.
 * @param {rootScope} $rootScope - Lets us access LocalForage events vis $on.
 * @param {Object} AddressBookService 
 * @param {Object} CountryService
 */
function ManageCtrl($rootScope, AddressBookService, CountryService) {
  const vm = this;
  vm.entries;

  $rootScope.$on('LocalForageModule.setItem', (event) => {
    vm.readEntries();
  });
  $rootScope.$on('LocalForageModule.removeItem', (event) => {
    vm.readEntries();
  });

  vm.readEntries = () => {
    AddressBookService.readEntries((err, entries) => {
      if (err) {
        console.log(err);
        return;
      }
      vm.entries = entries;
      return;
    });
  };

  vm.createEntry = (newEntry) => {
    AddressBookService.createEntry(newEntry, (err, createdEntry) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log('Successfuly created entry:', createdEntry);
      return;
    });
  };

  vm.updateEntry = (id, updated) => {
    AddressBookService.updateEntry(id, updated, (err, updated) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log('Successfully updated entry: ', updated);
      return;
    });
  };

  vm.deleteEntry = (entry) => {
    AddressBookService.deleteEntry(entry, (err, removedEntry) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log('Successfuly removed entry:', removedEntry);
      return;
    });
  };

  vm.getCountries = () => {
    vm.countries = CountryService.getCountries();
    return;
  };

  vm.setWorkingEntry = (entry) => {
    vm.workingEntry = entry;
    return;
  };

  vm.readEntries();
  vm.getCountries();
}

angular.module('AddressBook').controller('ManageCtrl', ManageCtrl);
